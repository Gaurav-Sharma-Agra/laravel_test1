          
**************************************************************************************************************************************

          (A) Go to git lab link- https://gitlab.com/Gaurav-Sharma-Agra/laravel_test1
          (B) clone project using git clone https://gitlab.com/Gaurav-Sharma-Agra/laravel_test1.git
          (C) Open any editor and go to lara_task root folder
          (D) Go to .env file which have in root directory and set mysql credentials like-
                DB_HOST,DB_PORT,DB_DATABASE(laravel_task),DB_USERNAME,DB_PASSWORD
          (E) Run these command in project root directory
                :-  composer update
                :-  php artisan optimize:clear
                :-  php artisan migrate
                :-  php artisan serve    and then go to http://127.0.0.1:8000/ URL
          (F) --Then click on register button and fill all fileds- username,gmail,password and click on register button.
                then you have login in successfully and see dashboard page. 
              --On dashboard page you see header title and two button, when you have click on logout button then you have logout
                and redirect to login page.
              -- and one git lab source button when you click on that button you redirect to git lab project directory
              -- and when you login or refresh the dashboard page you see one random though on page and on page have one 
                 refresh button when you click on it then refresh thought and see random new thought
              -- and you see 5 random thought in top 5 random thought section when you enter in dashboard page or login in 
                 then you see 5  diffrent thought every time and you refresh the page then also change thoughts
         (G) and hit http://127.0.0.1:8000/ URl the you see login page. and if you have already registred then enter gmail
             and password then you have login suucessfully if credentials are correct. and redirect to dashboard page.

             Note:- fully validated register and login page and authenticated user can access dashboard routes.
                    and when user register the create a web token in personal access token table and user logout then delete token according user id.

        (H) REGISTER, LOGIN AND LOGOUT API USING SACTUM WITH AUTHENTICATE USING TOEKN
                          Open postman or any other tool
            for register hit this http://localhost:8000/api/register URL with post request and assign three parameter name,email, password in json formate like this:- {
                                                "name":"username",
                                                "email":"useremail@gmail.com",
                                                "password":"userPass"
                                            }
            and after reposne return suucess message and return one token.

        (I) For login hit this http://localhost:8000/api/login URL with post request and assign two parameter email,password
            in jsom formate like this:- {
                                    "email":"gauravvvv@gmail.com",
                                    "password":"88888888"
                                }
            and after repsone return success message and return one token with that token you have logout.

        (J) For logout hit this http://localhost:8000/api/logout URL  and in header section key:accept value application/json 
            and second key:autherization value Bearer space token which you have got when login in like this-
            (Bearer 30|9zVYCAc1xWAegMpBBdC2NQFEzkhubXVHmaQALjjA585d1426) and you use wrong or without login token then return message unauthenticate.

        (K) For get 5 random thought using api hit this http://localhost:8000/api/getApiData URL and in header section key:accept value application/json and second key:autherization value Bearer space token which you have got when login in like this- (Bearer 30|9zVYCAc1xWAegMpBBdC2NQFEzkhubXVHmaQALjjA585d1426) and you use wrong or without login token then return message unauthenticate.
        and if token correct meand you are authenticate succefully then in reponse it return 5 random thoughs.

        (L) For test and run feature test case,run these command in root directory-
                composer require --dev laravel/dusk
                php artisan dusk:install
            and for run test case which have check register,login,logout and dashboard content and dashboard flow,run this command-
                php artisan dusk
                for run indivisual testcase run- php artisan dusk --filter=AuthFlowTest

****************************************************************************************************************************************


