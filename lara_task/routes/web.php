<?php

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [AuthController::class, 'login_view'])->name('login');
Route::post('post-login', [AuthController::class, 'postLogin'])->name('login.post'); 
Route::get('/register', [AuthController::class, 'register_view'])->name('register');
Route::post('post-registration', [AuthController::class, 'postRegistration'])->name('register.post'); 





// Athenticate user can access these routes
Route::middleware(['auth'])->group(function () {
    Route::get('dashboard', [AuthController::class, 'viewDashboard']); 
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::post('/dashboard/apiData',[AuthController::Class,'getApiDatas']);
});

