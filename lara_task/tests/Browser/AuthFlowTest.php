<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Exception;
class AuthFlowTest extends DuskTestCase
{
    public function testUserRegistrationLoginAndLogout()
    {
        $this->browse(function (Browser $browser) {
            $faker = \Faker\Factory::create();
            $username =  $faker->userName;
            $email=  $faker->safeEmail;
            $pass = $faker->password;
            // Registration
            $browser->visit('/register')
                    ->pause(500)
                    ->assertSee('Register')
                ->typeSlowly('#username', $username)
                ->typeSlowly('#email', $email)
                ->typeSlowly('#password', $pass)
                ->pause(500)
                ->press('#rButton')
                ->assertPathIs('/dashboard');
           
           #lognout
           $browser->press('#logout')
                    ->pause(500)
                    ->assertPathIs('/')
                    ->assertSee('Login');
            
           #login         
           $browser->pause(500)
                    ->typeSlowly('#username', $email)
                    ->typeSlowly('#password', $pass)
                    ->pause(100)
                    ->press('Login')
                    ->pause(100)
                    ->assertPathIs('/dashboard')
                    ->pause(100)
                    ->assertSee("Dashboard");
        // check content
            $browser->assertPresent("#my_list")
                    ->assertVisible('ul#my_list');
                    $listItems = $browser->elements('ul#my_list li');
                    // Count the list items
                    $listItemCount = count($listItems);
                    dump("Number of list items: $listItemCount"); 
                    $this->assertEquals(5, $listItemCount); 

            // check dashbaord
                $myvalaue_old = '';
                for ($x = 0; $x <= 3; $x++) {
                    $browser->press('#showdata')
                            ->pause(500);
                    $myvalaue = $browser->text("#showData");  
                    if ($myvalaue !=  $myvalaue_old){
                        $myvalaue_old = $myvalaue;
                        echo "CLICK PASSED ".$x;
                    }
                    else{
                        echo "OLD VALUE -> ".$myvalaue_old;
                        echo "NEW VALUE -> ".$myvalaue;
                        echo "TEST FAILD";
                        throw new Exception("Due to New value not init.");
                        
                    }
        
              }
         
        });
    }
}
