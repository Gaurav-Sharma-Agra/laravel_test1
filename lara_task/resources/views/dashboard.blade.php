<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <title>Dashboard</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-body-tertiary">
  <div class="container">
    <a class="navbar-brand me-2" href="#">
      <img
        src="download.png"
        height="25"
        alt="epic Logo"
        loading="lazy"
        style="margin-top: -1px;"
      />
    </a>

    <button
      data-mdb-collapse-init
      class="navbar-toggler"
      type="button"
      data-mdb-target="#navbarButtonsExample"
      aria-controls="navbarButtonsExample"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <i class="fas fa-bars"></i>
    </button>

    <div class="collapse navbar-collapse" id="navbarButtonsExample">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="#">Dashboard</a>
        </li>
      </ul>

      <div class="d-flex align-items-center">
        <a data-mdb-ripple-init type="button" href="{{ route('logout') }}" class="btn btn-primary me-3" id="logout">
          Logout
        </a>
        <a data-mdb-ripple-init type="button" href="https://gitlab.com/Gaurav-Sharma-Agra/laravel_test1" class="btn btn-success me-3">
          Git lab link 
        </a>
      </div>
    </div>
  </div>
</nav>

<!-- main content -->
<div class="card">
  <div class="card-header" style="margin-top:5%; background-color:#ccc;">
    <b>Thought API</b>
        <a class="pull-right btn btn-sm" id="showdata" href="javascript:void(0);" onclick="getApiData();" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><img src="refresh.png" alt="refresh" width="20" height="20">&nbsp;&nbsp;<b>Refresh</b></a>
  </div>
  <div class="card-body">
    <span id="showData" style="margin-left:45%; color:black;"></span>
  </div>
  <div class="card-footer" style="background-color:#ccc;">
    <span><b>Show Top 5 Quates:-</b></span>
   
     @foreach($randomQuotes as $quote)
      <ul id = "my_list">
        <li>{{ $quote['title'] }}</li>
      </ul>
    @endforeach
  </div>
</div>

<script>
$(document).ready(function(){
    $("#showData").html('<img src="loading.gif" alt="loading..." width="30" height="30">');
    getApiData();
});
  function getApiData() {
        $("#showData").html('<img src="loading.gif" alt="loading..." width="30" height="30">');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/dashboard/apiData",
            dataType: "json",
            success: function (data) {
                console.log(data['title']);
                $("#showData").html(data['title']);
            },
            error: function (error) {
                console.log("Error:", error);
            }
        });
    }
</script>
</body>
</html>