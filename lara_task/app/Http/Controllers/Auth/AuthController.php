<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;



class AuthController extends Controller
{
    public function login_view(){
        return view('login');
    }
    public function register_view(){
        return view('register');
    }

    public function postLogin(Request $request)
{
    $request->validate([
        'email' => 'required',
        'password' => 'required',
    ]);

    $credentials = $request->only('email', 'password');
    if (Auth::attempt($credentials)) {
        $user = User::where('email', $request->email)->first();
        $token = $user->createToken($request->email)->plainTextToken;

        $request->session()->put('token', $token);

        return redirect()->intended('dashboard')
            ->withSuccess('You have Successfully logged in');
    }

    return redirect("/")->withSuccess('Oops! You have entered invalid credentials');
}


    public function postRegistration(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')],
            'password' => 'required',
        ], [
            'name.required' => 'The username is required.',
            'email.required' => 'The email address is required.',
            'email.email' => 'Invalid email format.',
            'email.unique' => 'This email address is already registered.',
            'password.required' => 'The password is required.',
        ]);
        // dd($request->input('name'));
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);

        Auth::login($user);

        $token = $user->createToken($request->email)->plainTextToken;
    
        return redirect("dashboard")->with('token', $token)->with('message', 'You have Successfully registered');
    }

public function logout(){
    auth()->user()->tokens()->delete();
    Session::flush();
    Auth::logout();

    return Redirect('/');
}

    public function viewDashboard() {
        $apiUrl = "https://jsonplaceholder.typicode.com/posts";
        $response = Http::get($apiUrl);
    
        if ($response->successful()) {
            $quoteData = $response->json();
            $randomIndexes = array_rand($quoteData, 5);
            $randomQuotes = [];
            foreach ($randomIndexes as $index) {
                $randomQuotes[] = $quoteData[$index];
            }
    
            return view('dashboard', ['randomQuotes' => $randomQuotes]);
        } else {
            $errorMessage = "Failed to retrieve data from the API.";
            return response()->json(['error' => $errorMessage], $response->status());
        }
    }

    public function getApiDatas(){
        $apiUrl = "https://jsonplaceholder.typicode.com/posts";
        $response = Http::get($apiUrl);
        if ($response->successful()) {
            $quoteData = $response->json();
            $randomIndex = rand(0, 100);
            // dd(json_encode($quoteData[$randomIndex]));
            return json_encode($quoteData[$randomIndex]);
        } else {
            $errorMessage = "Failed to retrieve data from the API.";
            return response()->json(['error' => $errorMessage], $response->status());
        }
    }

}
